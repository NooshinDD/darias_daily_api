# darias_daily_api_v1
This project based on Python `3.8+`, Django `3.1+`, Celery `5+`, Redis`4+`

### Getting Started

Setup project with [docker-compose](https://docs.docker.com/compose/)  and [make](https://en.wikipedia.org/wiki/Make_(software))

```bash
$ make build
$ make up

# You may need to run django commands ONCE after up the project
# to install fixtures
$ make run loaddata challenge_labels.json
$ make run loaddata levels.json
$ make run streaks.json


```
For more useful commands see `MakeFile`,
for first login recommended use fake phone number see `settings.py`


### Third-party 
The project uses several third-party api that require their keys to be in the file ".project.env":
* `Sendgrid`- for managing mail-sending https://sendgrid.com/



