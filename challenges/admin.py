from django.contrib import admin
from django.db import models
from django.forms import Textarea
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from users.models import DariasUser, Streak

from challenges.models import Challenge, ChallengeApproval, DariasUserChallenge
from core.date_functions import yesterday_start
from core.statistic import add_points_by_category, add_points_by_complexity
from core.tasks import update_challenge_list


@admin.register(Challenge)
class ChallengeAdmin(admin.ModelAdmin):
    list_display = ("title", "category", "complexity", "created_at")
    fields = (
        "title",
        "category",
        "complexity",
        "description",
        "score",
        "photo",
        "is_active",
        "labels",
        "date",
    )
    exclude = (
        "id",
        "updated_at",
        "created_at",
    )
    list_filter = ("category", "complexity")
    actions = ("disable", "delete_queryset")

    formfield_overrides = {
        models.CharField: {"widget": Textarea(attrs={"rows": 2, "cols": 25})},
    }

    def disable(self, request, queryset):
        return queryset.update(is_active=False)

    def delete_queryset(self, request, queryset):
        return queryset.delete()

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(self, request, obj, form, change):
        if not form.data["date"]:
            return super().save_model(request, obj, form, change)
        update_challenge_list.delay(obj.id)
        return super().save_model(request, obj, form, change)

    disable.short_description = "Disable selected"
    delete_queryset.short_description = "Delete selected"


@admin.register(ChallengeApproval)
class ChallengeApprovalsAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            "Challenge info",
            {
                "fields": (
                    "challenge_title",
                    "challenge_description",
                    "challenge_photo",
                )
            },
        ),
        (
            "Answer",
            {"fields": ("comment", "images", "last_image")},
        ),
    )
    readonly_fields = (
        "challenge_title",
        "challenge_description",
        "challenge_photo",
        "comment",
        "images",
        "last_image",
    )
    list_display = ("challenge", "user", "category", "level", "created_at")

    list_filter = ["challenge__challenge__complexity", "challenge__challenge__category"]

    actions = ["accept", "reject"]

    def user(self, obj: ChallengeApproval):
        return obj.challenge.user

    def category(self, obj: ChallengeApproval):
        return obj.challenge.challenge.category

    def level(self, obj: ChallengeApproval):
        return obj.challenge.challenge.complexity

    def images(self, obj):
        photo = obj.photos.all().first()
        return mark_safe('<img src="%s" width="400" height="400" />\n' % photo.photo_url)

    def last_image(self, obj):
        if len(obj.photos.all()) > 1:
            photo = obj.photos.all().last()
        else:
            return ""
        return mark_safe('<img src="%s" width="400" height="400" />\n' % photo.photo_url)

    def challenge_title(self, obj):
        return obj.challenge.challenge.title

    def challenge_description(self, obj):
        # splits text after 55 symbols
        def _insert_space(string, index):
            return string[:index] + "\n" + string[index:]

        text = obj.challenge.challenge.description
        for i, _ in enumerate(text):
            if not i % 55 and i != 0:
                text = _insert_space(text, i)
        return text

    def challenge_photo(self, obj):
        return mark_safe(
            '<img src="%s" width="400" height="400" />' % obj.challenge.challenge.photo_url
        )

    challenge_photo.short_description = "Image"
    last_image.short_description = ""

    def get_queryset(self, request):
        return (
            super(ChallengeApprovalsAdmin, self)
            .get_queryset(request)
            .filter(challenge__status=DariasUserChallenge.PENDING_STATUS)
            .select_related("challenge")
        )

    def challenge_(self, obj):
        user_challenge_id = obj.challenge.challenge.pk
        info = (Challenge._meta.app_label, Challenge._meta.model_name)
        url = reverse("admin:{}_{}_change".format(*info), args=(user_challenge_id,))

        return format_html('<a href="{url}">{text}</a>'.format(url=url, text=obj.challenge))

    def has_add_permission(self, request):
        return False

    def change_status(self, request, obj: ChallengeApproval, status: str):
        obj.challenge.status = status
        obj.challenge.save(update_fields=["status"])
        self.message_user(request, f"Request marked as '{status}'")

    def response_change(self, request, obj: ChallengeApproval):
        if "approved" in request.POST:
            self.give_points(obj.challenge.challenge.score, obj.challenge.user, obj.challenge)
            self.change_status(request=request, obj=obj, status=DariasUserChallenge.APPROVED_STATUS)
            return HttpResponseRedirect("/admin/challenges/challengeapproval")
        if "rejected" in request.POST:
            self.change_status(request=request, obj=obj, status=DariasUserChallenge.REJECTED_STATUS)
            return HttpResponseRedirect("/admin/challenges/challengeapproval")
        if "close" in request.POST:
            return HttpResponseRedirect("/admin/challenges/challengeapproval")
        return super().response_change(request, obj)

    def give_points(
        self, challenge_points, user: DariasUser, current_challenge: DariasUserChallenge
    ):
        # method that gets users multipliers according to streaks
        def _get_multipliers(current_challenge):
            try:
                yesterday = (
                    current_challenge.user.challenges.filter(
                        status=DariasUserChallenge.APPROVED_STATUS,
                        start_date__range=[yesterday_start(timezone.now()), timezone.now()],
                    )
                    .first()
                    .start_date.day
                )
            except AttributeError:
                yesterday = 0
            if current_challenge.start_date.day - yesterday == 1:
                current_challenge.user.streak_days += 1
            else:
                current_challenge.user.streak_days = 1
            current_challenge.user.save()
            return Streak.objects.filter(days__range=[1, user.streak_days])

        multipliers = _get_multipliers(current_challenge)
        if not multipliers:
            user.streaks.filter(user=user).delete()
            total_points = challenge_points
        else:
            multiplier = multipliers.last().multiplier
            user.streaks.add(multiplier)
            total_points = challenge_points * multiplier
        current_challenge.points = total_points
        current_challenge.save(
            update_fields=[
                "points",
            ]
        )
        user.total_points += total_points
        add_points_by_complexity(current_challenge)
        add_points_by_category(current_challenge)
        user.save(update_fields=["total_points", "streak_days"])

    def reject(self, request, queryset):
        for approval in queryset:
            self.change_status(request, obj=approval, status=DariasUserChallenge.REJECTED_STATUS)

    def accept(self, request, queryset):
        for approval in queryset:
            self.give_points(
                approval.challenge.challenge.score, approval.challenge.user, approval.challenge
            )
            self.change_status(request, obj=approval, status=DariasUserChallenge.APPROVED_STATUS)
