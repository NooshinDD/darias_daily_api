# Generated by Django 3.2 on 2021-04-07 12:57

import datetime

from django.db import migrations, models
from django.utils.timezone import utc

import challenges.models
import core.aws_storages
import validators


class Migration(migrations.Migration):

    dependencies = [
        ("challenges", "0008_change_category_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="approvalphoto",
            name="photo",
            field=models.ImageField(
                default=None,
                help_text="Max file size is 2.0MB",
                null=True,
                storage=core.aws_storages.MediaStorage(),
                upload_to=challenges.models.approval_photo_file_name,
                validators=[validators.validate_image],
                verbose_name="photo",
            ),
        ),
        migrations.AlterField(
            model_name="challenge",
            name="photo",
            field=models.ImageField(
                default=None,
                help_text="Max file size is 2.0MB",
                null=True,
                storage=core.aws_storages.MediaStorage(),
                upload_to=challenges.models.challenge_photo_file_name,
                validators=[validators.validate_image],
                verbose_name="photo",
            ),
        ),
        migrations.AlterField(
            model_name="dariasuserchallenge",
            name="end_date",
            field=models.DateTimeField(
                default=datetime.datetime(2021, 4, 7, 23, 59, 59, tzinfo=utc),
                help_text='Challange stop date. Example: "2019-07-28T12:45:59+01:00"',
                verbose_name="end date",
            ),
        ),
        migrations.AlterField(
            model_name="dariasuserchallenge",
            name="start_date",
            field=models.DateTimeField(
                default=datetime.datetime(2021, 4, 7, 0, 0, tzinfo=utc),
                help_text='Challange start date. Example: "2019-05-28T21:34:11+01:00"',
                verbose_name="start date",
            ),
        ),
    ]
