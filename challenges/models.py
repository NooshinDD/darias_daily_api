from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from challenges.mixins import AutoLogMixin
from core.aws_storages import MediaStorage
from core.date_functions import end_of_the_day, start_of_the_day
from validators import validate_image


def challenge_photo_file_name(instance, filename):
    return "/".join(["content", "challenges", instance.title, filename])


def approval_photo_file_name(instance, filename):
    return "/".join(["content", "challenges", "approvals", str(instance.approval.id), filename])


class ChallengeLabel(models.Model):

    CAR = "CAR"
    HOME = "HOME"
    NO_POSSESSIONS = "NO_POSSESSIONS"
    VEGETARIAN = "VEGETARIAN"
    VEGAN = "VEGAN"
    OMNIVORE = "OMNIVORE"

    LABELS_NAMES = (
        (CAR, "car"),
        (HOME, "home"),
        (NO_POSSESSIONS, "no possessions"),
        (VEGETARIAN, "vegetarian"),
        (VEGAN, "vegan"),
        (OMNIVORE, "omnivore"),
    )

    name = models.CharField(default="no possessions", max_length=18)
    user = models.ManyToManyField(
        "users.DariasUser",
        related_name="users_labels",
        blank=True,
    )

    def __str__(self):
        return self.name


class Challenge(AutoLogMixin):

    EASY_COMPLEXITY = "EASY_C"
    MEDIUM_COMPLEXITY = "MEDIUM_C"
    HARD_COMPLEXITY = "HARD_C"

    CHALLENGE_COMPLEXITY = (
        (EASY_COMPLEXITY, "easy"),
        (MEDIUM_COMPLEXITY, "medium"),
        (HARD_COMPLEXITY, "hard"),
    )

    FOOD_IMPACT_CATEGORY = "FOOD_IMPACT_C"
    ENERGY_CATEGORY = "ENERGY_C"
    PLANTS_ANIMALS_CATEGORY = "PLANTS_ANIMALS_C"
    WATER_CATEGORY = "WATER_C"

    CHALLENGE_CATEGORY = (
        (FOOD_IMPACT_CATEGORY, "Food impact"),
        (ENERGY_CATEGORY, "Energy"),
        (PLANTS_ANIMALS_CATEGORY, "Plants&Animals"),
        (WATER_CATEGORY, "Water"),
    )
    title = models.CharField(max_length=64, default="")
    description = models.CharField(max_length=512, default="")
    photo = models.ImageField(
        (_("photo")),
        null=True,
        default=None,
        upload_to=challenge_photo_file_name,
        validators=[validate_image],
        help_text="Max file size is %sMB" % str(settings.MAX_FILE_SIZE),
        storage=MediaStorage(),
    )
    score = models.SmallIntegerField(default=0)
    complexity = models.CharField(
        verbose_name=(_("level")),
        choices=CHALLENGE_COMPLEXITY,
        default=EASY_COMPLEXITY,
        max_length=18,
    )
    category = models.CharField(
        choices=CHALLENGE_CATEGORY,
        default="",
        max_length=26,
    )

    labels = models.ManyToManyField("ChallengeLabel", related_name="labels")
    date = models.DateField(null=True, blank=True, default=None)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    @property
    def photo_url(self):
        if self.photo:
            return self.photo.url

    def clean(self):
        if self.date and self.date <= timezone.now().date():
            raise ValidationError("Future date only")


class DariasUserChallenge(models.Model):
    NEW_STATUS = "NEW_S"
    PICKED_STATUS = "PICKED_S"
    PENDING_STATUS = "PENDING_S"
    APPROVED_STATUS = "APPROVED_S"
    REJECTED_STATUS = "REJECTED_S"

    CHALLENGE_STATUSES = (
        (NEW_STATUS, "new"),
        (PICKED_STATUS, "picked"),
        (PENDING_STATUS, "pending"),
        (APPROVED_STATUS, "approved"),
        (REJECTED_STATUS, "rejected"),
    )

    start_date = models.DateTimeField(
        verbose_name=_("start date"),
        blank=False,
        help_text=_('Challange start date. Example: "2019-05-28T21:34:11+01:00"'),
        default=start_of_the_day(timezone.now().utcnow()),
    )
    end_date = models.DateTimeField(
        verbose_name=_("end date"),
        blank=False,
        help_text=_('Challange stop date. Example: "2019-07-28T12:45:59+01:00"'),
        default=end_of_the_day(timezone.now().utcnow()),
    )
    challenge = models.ForeignKey(
        "Challenge", related_name="user_challenge", on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        "users.DariasUser", related_name="challenges", on_delete=models.CASCADE
    )
    status = models.CharField(choices=CHALLENGE_STATUSES, default=NEW_STATUS, max_length=18)
    points = models.DecimalField(default=0, decimal_places=3, max_digits=10)

    def __str__(self):
        return self.challenge.title

    @property
    def date(self):
        return self.end_date


class ChallengeApproval(AutoLogMixin):
    challenge = models.OneToOneField(
        "DariasUserChallenge",
        related_name="challenge_approval",
        on_delete=models.CASCADE,
    )
    comment = models.CharField(max_length=165, default="")


class ApprovalPhoto(models.Model):
    photo = models.ImageField(
        (_("photo")),
        null=True,
        default=None,
        upload_to=approval_photo_file_name,
        validators=[validate_image],
        help_text="Max file size is %sMB" % str(settings.MAX_FILE_SIZE),
        storage=MediaStorage(),
    )
    approval = models.ForeignKey(
        "challengeapproval", related_name="photos", on_delete=models.CASCADE
    )

    @property
    def photo_url(self):
        if self.photo:
            return self.photo.url
