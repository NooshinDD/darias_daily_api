from django.db.models import Case, IntegerField, Value, When
from django.utils import timezone
from rest_framework import serializers

from challenges.models import Challenge, ChallengeApproval, ChallengeLabel, DariasUserChallenge
from core.date_functions import days_back, days_forward, end_of_the_day, start_of_the_day
from core.generators import date_generator, get_by_complexity, get_users_queryset


class ChallengeLabelSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    name = serializers.CharField(required=False)

    class Meta:
        model = ChallengeLabel
        fields = ("id", "name")


class ChallengeSerializer(serializers.ModelSerializer):
    labels = ChallengeLabelSerializer(many=True, required=False)

    class Meta:
        model = Challenge
        fields = (
            "id",
            "title",
            "description",
            "photo",
            "labels",
            "score",
            "complexity",
            "category",
        )


class ChallengeLabelListSerializer(serializers.Serializer):
    labels = serializers.ListSerializer(child=ChallengeLabelSerializer())

    class Meta:
        fields = ("id",)

    def update(self, instance, validated_data):
        instance.users_labels.clear()
        for item in self.validated_data["labels"]:
            instance.users_labels.add(ChallengeLabel.objects.get(pk=item["id"]))
        return instance


class UserChallengeSerializer(serializers.ModelSerializer):
    challenge = ChallengeSerializer()

    class Meta:
        model = DariasUserChallenge
        fields = ("id", "challenge", "status", "start_date", "end_date")


class UserChallengePickSerializer(serializers.ModelSerializer):
    challenge = serializers.PKOnlyObject(pk=int)

    class Meta:
        model = DariasUserChallenge
        fields = ("id", "challenge", "status", "start_date", "end_date")


class UserChallengeListSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(required=False, format="%Y-%m-%d")
    challenges = serializers.SerializerMethodField("get_challenges")

    class Meta:
        model = DariasUserChallenge
        fields = ("date", "challenges")

    def get_challenges(self, obj):
        challenges = (
            DariasUserChallenge.objects.filter(end_date=obj.end_date, user=obj.user)
            .annotate(
                o=Case(
                    When(
                        status__in=[
                            DariasUserChallenge.APPROVED_STATUS,
                            DariasUserChallenge.PENDING_STATUS,
                            DariasUserChallenge.REJECTED_STATUS,
                        ],
                        then=Value(0),
                    ),
                    When(challenge__complexity=Challenge.EASY_COMPLEXITY, then=Value(1)),
                    When(challenge__complexity=Challenge.MEDIUM_COMPLEXITY, then=Value(2)),
                    When(challenge__complexity=Challenge.HARD_COMPLEXITY, then=Value(3)),
                    output_field=IntegerField(),
                )
            )
            .order_by("o")
            .prefetch_related("challenge", "challenge__labels")
        )

        challenges_serializer = UserChallengeSerializer(challenges, context=self.context, many=True)
        return challenges_serializer.data


class ApprovalSerializer(serializers.ModelSerializer):
    photo = serializers.ImageField(required=True)
    additional_photo = serializers.ImageField(required=False)
    comment = serializers.CharField(max_length=30)

    class Meta:
        model = ChallengeApproval
        fields = ("comment", "photo", "additional_photo")


class GenerateChallengeSerializer(serializers.ModelSerializer):
    challenges = serializers.ListSerializer(child=UserChallengeSerializer(), required=False)

    class Meta:
        model = DariasUserChallenge
        fields = ("challenges",)

    def create(self, user):
        def _generate_challenges(dates):
            challenges = []
            for day in dates:
                queryset = get_by_complexity(
                    get_users_queryset(Challenge.objects.filter(is_active=True), user).order_by(
                        "?"
                    ),
                    user,
                )
                for i in queryset:
                    challenges.append(
                        DariasUserChallenge(
                            challenge=i,
                            user=user,
                            start_date=start_of_the_day(day),
                            end_date=end_of_the_day(day),
                        )
                    )

            DariasUserChallenge.objects.bulk_create(challenges)

        days_to_generate = date_generator(start_of_the_day(timezone.now()), 15)
        user_challenges = DariasUserChallenge.objects.filter(user=user)
        challenges_pack = user_challenges.filter(
            end_date__range=[
                start_of_the_day(timezone.now()),
                end_of_the_day(days_to_generate[-1]),
            ],
        ).order_by("start_date")
        if not challenges_pack:
            _generate_challenges(days_to_generate)
        if challenges_pack.last().start_date != days_to_generate[-1]:
            empty_days = days_to_generate[-1] - challenges_pack.last().start_date
            days_without_challenges = date_generator(
                start_of_the_day(days_to_generate[-empty_days.days]), empty_days.days
            )
            _generate_challenges(days_without_challenges)
        challenges_in_range = user_challenges.filter(
            start_date__range=[days_back(14), days_forward(14)]
        ).order_by("start_date")
        return challenges_in_range


class EmptySerializer(serializers.Serializer):
    pass
