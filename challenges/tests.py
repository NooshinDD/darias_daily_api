import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.utils import timezone

from PIL import Image
from six import BytesIO

from users.models import DariasUser, Level, Statistic

from challenges.models import Challenge, ChallengeApproval, ChallengeLabel, DariasUserChallenge
from challenges.serializers import ChallengeLabelListSerializer
from core.date_functions import days_back
from core.generators import date_generator


class ChallengesTestCase(TestCase):
    email = "testuser@example.com"
    password = "Password1234!"
    admin_email = "testadminuser@example.com"
    challenge_data = {
        "title": "active",
        "description": "test",
        "photo": "",
        "score": 15,
        "category": "FOOD_IMPACT_C",
        "date": "2021-03-04",
    }

    def setUp(self):
        self.level = Level.objects.create(name="starter", score=0)
        self.admin = DariasUser.objects.create_superuser(self.admin_email, self.password)
        self.user = DariasUser.objects.create_user(self.email, password=self.password)
        self.label_1 = ChallengeLabel.objects.create(name=ChallengeLabel.OMNIVORE)
        self.label_2 = ChallengeLabel.objects.create(name=ChallengeLabel.CAR)
        self.user.users_labels.set(
            [
                self.label_1,
            ]
        )
        self.user.first_name = "John"
        self.user.last_name = "Doe"
        self.user.birthday_date = "2021-02-02"
        self.user.statistic = Statistic.objects.create(
            user=self.user, login_streak_update=timezone.now().date()
        )
        self.user.save()
        self.challenge = Challenge.objects.create(
            **self.challenge_data, complexity=Challenge.EASY_COMPLEXITY, is_active=True
        )
        self.challenge.labels.set(
            [
                self.label_1,
                self.label_2,
            ],
        )
        self.challenge.save()
        self.second_challenge = Challenge.objects.create(
            **self.challenge_data, complexity=Challenge.MEDIUM_COMPLEXITY, is_active=True
        )
        self.second_challenge.labels.set(
            [
                self.label_1,
                self.label_2,
            ],
        )
        self.second_challenge.save()
        self.third_challenge = Challenge.objects.create(
            **self.challenge_data, complexity=Challenge.HARD_COMPLEXITY, is_active=True
        )
        self.third_challenge.labels.set(
            [
                self.label_1,
                self.label_2,
            ],
        )
        self.third_challenge.save()

    def _login(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + resp.json()["access"]
        self.assertEquals(200, resp.status_code)

    def test_challenge_labels_assign(self):
        self._login()
        resp = self.client.get("/challenges/labels/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(2, len(resp.json()))
        data = {
            "labels": [
                {"id": self.label_1.id},
            ]
        }
        d = json.dumps(data)
        serializer = ChallengeLabelListSerializer(data=data, instance=self.user)
        serializer.is_valid(raise_exception=True)
        # assign label
        resp = self.client.post("/challenges/labels/", data=d, content_type="application/json")
        self.assertEquals(200, resp.status_code)
        # check is it assigned
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(self.label_1.id, resp.json()["users_labels"][0]["id"])
        # delete label
        resp = self.client.post(
            "/challenges/labels/", data={"labels": []}, content_type="application/json"
        )
        self.assertEquals(200, resp.status_code)
        # check is deleted
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals([], resp.json()["users_labels"])

    def test_challenge(self):
        self._login()
        # check if challenge is available
        resp = self.client.get("/challenges/{}/".format(self.challenge.id))
        self.assertEquals(200, resp.status_code)
        self.assertEquals(resp.json()["id"], self.challenge.id)

    def test_admin_create_challenge(self):
        self.client.login(username=self.admin.username, password=self.password)
        image = BytesIO()
        Image.new("RGB", (100, 100)).save(image, "JPEG")
        image.seek(0)
        photo = SimpleUploadedFile("image.jpg", image.getvalue())
        resp = self.client.post(
            "/admin/challenges/challenge/add/",
            {
                "title": "sss",
                "description": "sss",
                "photo": photo,
                "category": "sss",
                "score": 1,
                "is_active": False,
                "labels": [],
                "date": "",
            },
            follow=True,
        )
        self.assertEquals(resp.status_code, 200)

    def test_challenge_take(self):
        self._login()
        # create_approval
        approvals = ChallengeApproval.objects.all()
        image = BytesIO()
        Image.new("RGB", (100, 100)).save(image, "JPEG")
        image.seek(0)
        photo = SimpleUploadedFile("image.jpg", image.getvalue())
        self.assertEqual(len(approvals), 0)
        user_challenge = DariasUserChallenge.objects.create(
            user=self.user, challenge=self.challenge
        )
        resp = self.client.post(
            f"/challenges/me/{user_challenge.id}/approve/",
            data={"comment": "text", "photo": photo, "second": ""},
        )
        approvals = ChallengeApproval.objects.all()
        self.assertEquals(approvals.count(), 1)
        self.assertEquals(200, resp.status_code)
        # check if it's pending
        resp = self.client.get("/challenges/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(resp.json()[0]["status"], DariasUserChallenge.PENDING_STATUS)

    def test_challenges_today(self):
        self._login()
        # check that there are some available challenges
        resp = self.client.get(path="/challenges/")
        self.assertEquals(len(resp.json()), 3)
        # generate challenges
        resp = self.client.post(path="/challenges/me/generate/")
        first_users_challenge_ids = [
            [j["id"] for j in i["challenges"]] for i in resp.json() if i["challenges"]
        ]
        self.assertEquals(200, resp.status_code)
        # generate challenges for today
        resp = self.client.post(path="/challenges/me/today/")
        self.assertEquals(200, resp.status_code)
        # check that generating was successful and new challenges was created
        resp = self.client.post(path="/challenges/me/generate/")
        second_users_challenge_ids = [
            [j["id"] for j in i["challenges"]] for i in resp.json() if i["challenges"]
        ]
        self.assertNotEquals(first_users_challenge_ids, second_users_challenge_ids)
        self.assertEquals(200, resp.status_code)

    def test_challenges_generate(self):
        self._login()
        resp = self.client.post("/challenges/me/generate/")
        self.assertEquals(200, resp.status_code)
        response_dates = [i["date"] for i in resp.json()]
        dates = [str(date.date()) for date in date_generator(days_back(14), 29)]
        response_challenges = [len(i["challenges"]) for i in resp.json() if i["challenges"]]
        user_challenges = DariasUserChallenge.objects.all()
        self.assertEquals(sum(response_challenges), user_challenges.count())
        self.assertEquals(dates, response_dates)
        self.assertEquals(len(resp.json()), 29)
        # check that there is no changes if we do same thing
        resp = self.client.post("/challenges/me/generate/")
        self.assertEquals(200, resp.status_code)
        user_challenges_2 = DariasUserChallenge.objects.all()
        response_dates_2 = [i["date"] for i in resp.json()]
        response_challenges_2 = [len(i["challenges"]) for i in resp.json() if i["challenges"]]
        self.assertEquals(user_challenges_2.count(), user_challenges.count())
        self.assertEquals(response_challenges_2, response_challenges)
        self.assertEquals(response_dates, response_dates_2)
        self.assertEquals(len(resp.json()), 29)
