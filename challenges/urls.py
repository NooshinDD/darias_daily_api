from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import ChallengeLabelsViewSet, ChallengeViewSet, DariasUserChallengeViewSet

router = DefaultRouter()
router.register("me", DariasUserChallengeViewSet, basename="users_challenge")
router.register("labels", ChallengeLabelsViewSet, basename="label")
router.register("", ChallengeViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
