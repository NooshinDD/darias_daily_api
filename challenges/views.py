from django.utils import timezone
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from challenges.models import (
    ApprovalPhoto,
    Challenge,
    ChallengeApproval,
    ChallengeLabel,
    DariasUserChallenge,
)
from challenges.serializers import (
    ApprovalSerializer,
    ChallengeLabelListSerializer,
    ChallengeLabelSerializer,
    ChallengeSerializer,
    EmptySerializer,
    GenerateChallengeSerializer,
    UserChallengeListSerializer,
    UserChallengeSerializer,
)
from core.date_functions import days_back, end_of_the_day, start_of_the_day
from core.generators import date_generator, get_by_complexity, get_users_queryset
from core.tasks import update_users_statistic


class ChallengeViewSet(viewsets.ModelViewSet):
    http_method_names = ("get",)
    queryset = Challenge.objects.filter(is_active=True)
    permission_classes = (IsAuthenticated,)
    serializer_class = ChallengeSerializer


class ChallengeLabelsViewSet(viewsets.GenericViewSet):
    http_method_names = ("post", "get")
    queryset = ChallengeLabel.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = ChallengeLabelSerializer

    def list(self, request, *args, **kwargs):
        """gives list of active interests"""
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "labels": openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={"id": openapi.Schema(type=openapi.TYPE_INTEGER)},
                    ),
                    description="List of Labels id, Empty list will erase all labels",
                ),
            },
        ),
    )
    def create(self, request, *args, **kwargs):
        serializer = ChallengeLabelListSerializer(
            data=request.data,
            instance=request.user,
        )
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(status=status.HTTP_200_OK)


class DariasUserChallengeViewSet(viewsets.GenericViewSet):
    serializer_class = UserChallengeListSerializer
    permission_classes = (IsAuthenticated,)
    queryset = DariasUserChallenge.objects.all()
    parser_classes = (MultiPartParser, FormParser)
    lookup_field = "pk"

    @swagger_auto_schema(
        methods=[
            "post",
        ]
    )
    @action(
        methods=[
            "post",
        ],
        detail=False,
        serializer_class=EmptySerializer,
    )
    def today(self, request, *args, **kwargs):
        user = request.user
        today_challenges = self.queryset.filter(user=user, end_date=end_of_the_day(timezone.now()))
        if challenges_in_progress := [
            i.id for i in today_challenges if i.status != DariasUserChallenge.NEW_STATUS
        ]:
            return Response(
                data={"message": f"Challenge {challenges_in_progress} is in pending status"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        today_challenges.delete()
        queryset = get_by_complexity(
            get_users_queryset(Challenge.objects.filter(is_active=True).order_by("?"), user), user
        )
        new_challenges = [
            (
                DariasUserChallenge(
                    challenge=i,
                    user=user,
                    start_date=start_of_the_day(start_of_the_day(timezone.now())),
                    end_date=end_of_the_day(timezone.now()),
                )
            )
            for i in queryset
        ]

        DariasUserChallenge.objects.bulk_create(new_challenges)
        return Response(status=status.HTTP_200_OK)

    def list(self, request, *args, **kwargs):
        """Lists of users challenges in past in approved and rejected,pending, statuses """
        queryset = (
            self.queryset.filter(
                user=request.user,
                status__in=[
                    DariasUserChallenge.APPROVED_STATUS,
                    DariasUserChallenge.REJECTED_STATUS,
                    DariasUserChallenge.PENDING_STATUS,
                ],
            )
            .prefetch_related("challenge", "challenge__labels")
            .order_by("-end_date")
        )
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = UserChallengeSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = UserChallengeSerializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=ApprovalSerializer(),
        responses={200: ApprovalSerializer()},
    )
    @action(methods=["post"], detail=True)
    def approve(self, request, pk=None):
        """creating approval"""
        files = request.FILES
        serializer = ApprovalSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_challenges = self.queryset.filter(
            user=request.user, end_date=end_of_the_day(timezone.now())
        )
        if list := [
            [i.id, i.status] for i in user_challenges if i.status != DariasUserChallenge.NEW_STATUS
        ]:
            return Response(
                data={
                    "message": f"Challenge for today with id {list[0][0]} is in {list[0][1]} status"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        try:
            user_challenge = user_challenges.get(pk=pk, status=DariasUserChallenge.NEW_STATUS)
        except DariasUserChallenge.DoesNotExist:
            return Response(
                data={"message": f"User Challenge {pk} not found"},
                status=status.HTTP_404_NOT_FOUND,
            )
        approval, created = ChallengeApproval.objects.get_or_create(challenge=user_challenge)
        if not created:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        approval.comment = serializer.data["comment"]
        approval.challenge.status = DariasUserChallenge.PENDING_STATUS
        approval.challenge.save()
        approval.save()

        if len(files) == 2:
            photo = files["additional_photo"]
            photo = ApprovalPhoto.objects.create(approval=approval, photo=photo)
            approval.photos.add(photo)
        photo = request.FILES["photo"]
        photo = ApprovalPhoto.objects.create(approval=approval, photo=photo)
        approval.photos.add(photo)
        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(
        methods=[
            "post",
        ],
    )
    @action(
        methods=[
            "post",
        ],
        detail=False,
        serializer_class=EmptySerializer,
    )
    def generate(self, request, *args, **kwargs):
        """generating challenges by date"""
        update_users_statistic.delay(request.user.id)
        serializer = GenerateChallengeSerializer()
        users_challenges = serializer.create(user=request.user)
        challenge_list = []
        [
            challenge_list.append(i)
            for i in users_challenges
            if i.end_date not in [i.end_date for i in challenge_list]
        ]
        serializer = UserChallengeListSerializer(
            data=challenge_list, context={"request": request}, many=True
        )
        serializer.is_valid()
        new_serializer_data = list(serializer.data)
        dates_to_response = [str(i.date()) for i in date_generator(days_back(14), 29)]
        for i in dates_to_response:
            if i not in [x["date"] for x in serializer.data]:
                new_serializer_data.append({"date": i, "challenges": []})
        new_serializer_data.sort(key=lambda x: x["date"])
        return Response(new_serializer_data, status=status.HTTP_200_OK)
