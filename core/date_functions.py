import datetime

from django.utils import timezone


def yesterday_start(today):
    return start_of_the_day(today - datetime.timedelta(days=1))


def yesterday_end(today):
    return end_of_the_day(today - datetime.timedelta(days=1))


def days_back(days):
    return timezone.now() - datetime.timedelta(days=days)


def days_forward(days):
    return timezone.now() + datetime.timedelta(days=days)


def start_of_the_day(date):
    return date.replace(
        hour=0,
        minute=0,
        second=0,
        microsecond=0,
        tzinfo=timezone.utc,
    )


def end_of_the_day(date):
    return date.replace(hour=23, minute=59, second=59, microsecond=0, tzinfo=timezone.utc)
