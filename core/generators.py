import datetime

from users.models import DariasUser

from challenges.models import Challenge


def item_by_complexity(queryset, complexity, user):
    if qs := queryset.filter(complexity=complexity):
        item = qs.first()
    else:
        qs = (
            Challenge.objects.all()
            .filter(complexity=complexity, is_active=True, labels__in=user.users_labels.all())
            .all()
            .order_by("?")
        )
        item = qs.first()
    return item


def get_by_complexity(queryset, user):
    fin_qs = []
    if easy_challenge := item_by_complexity(queryset, Challenge.EASY_COMPLEXITY, user):
        fin_qs.append(easy_challenge)
    if medium_challenge := item_by_complexity(queryset, Challenge.MEDIUM_COMPLEXITY, user):
        fin_qs.append(medium_challenge)
    if hard_challenge := item_by_complexity(queryset, Challenge.HARD_COMPLEXITY, user):
        fin_qs.append(hard_challenge)
    if not fin_qs:
        fin_qs = []
    return fin_qs


def get_users_queryset(queryset, user: DariasUser):
    queryset_by_labels = (queryset.filter(labels__in=user.users_labels.all())).order_by("?")
    if queryset_by_labels.count() <= 2:
        queryset_by_labels = queryset
    queryset_by_existance = queryset_by_labels.exclude(
        pk__in=[i[0] for i in user.challenges.values_list("challenge_id")]
    )
    if queryset_by_existance.count() <= 2:
        queryset_by_existance = queryset_by_labels
    return queryset_by_existance


def date_generator(form_date, numdays):
    return [form_date + datetime.timedelta(days=x) for x in range(numdays)]
