import secrets
import string


def generate_secret(length: int) -> str:
    return "".join(secrets.choice(string.hexdigits) for _ in range(length))
