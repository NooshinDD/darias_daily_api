import os
from datetime import timedelta

from celery.schedules import crontab

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


SECRET_KEY = "pk#uqu2+zr$&iwo6x5_-7xcz80cf(sm66rw58w80pxk4r3zc$e"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get("DEBUG") in ["True", "true"]
ALLOWED_HOSTS = ["*"]
ENVIRONMENT_NAME = os.environ.get("ENVIRONMENT_NAME", "staging")


# Application definition

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "sqlmiddleware",
    "storages",
    "rest_framework",
    "rest_framework_simplejwt.token_blacklist",
    "djrill",
    "djoser",
    "drf_yasg",
    "users",
    "challenges",
    "rewards",
    "core",
]
SITE_ID = 1

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "social_core.backends.google.GoogleOAuth2",
    "social_core.backends.apple.AppleIdAuth",
)

# GOOGLE
SOCIAL_GOOGLE_CLIENT_ID = "383611819673-gu8ghv8i1ss0hrrh01nh4kgvjp2345eg.apps.googleusercontent.com"
SOCIAL_GOOGLE_CLIENT_SECRET = "4R_VEAm4fKLq41fusSSVYR50"
SOCIAL_GOOGLE_CLIENT_TEMP_PASSWORD = "12345"

# APPLE
SOCIAL_AUTH_APPLE_ID_CLIENT = "..."  # Your client_id com.application.your, aka "Service ID"
SOCIAL_AUTH_APPLE_ID_TEAM = "..."  # Your Team ID, ie K2232113
SOCIAL_AUTH_APPLE_ID_KEY = "..."  # Your Key ID, ie Y2P99J3N81K
SOCIAL_AUTH_APPLE_ID_SECRET = """
-----BEGIN PRIVATE KEY-----
MIGTAgE.....
-----END PRIVATE KEY-----"""
SOCIAL_AUTH_APPLE_ID_SCOPE = ["email", "name"]
SOCIAL_AUTH_APPLE_ID_EMAIL_AS_USERNAME = True

ROOT_URLCONF = "urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "../templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "wsgi.application"

SWAGGER_SETTINGS = {
    "SECURITY_DEFINITIONS": {
        "Bearer": {"type": "apiKey", "name": "Authorization", "in": "header"},
    },
    "USE_SESSION_AUTH": True,
    "JSON_EDITOR": True,
    "APIS_SORTER": "alpha",
}

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": ("rest_framework.permissions.IsAuthenticated",),
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
    "DEFAULT_RENDERER_CLASSES": ("rest_framework.renderers.JSONRenderer",),
    "DEFAULT_PARSER_CLASSES": (
        "rest_framework.parsers.JSONParser",
        "rest_framework.parsers.MultiPartParser",
    ),
}


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("POSTGRES_DB"),
        "USER": os.environ.get("POSTGRES_USER"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD"),
        "HOST": os.environ.get("POSTGRES_HOST"),
        "PORT": 5432,
    }
}


CELERY_BEAT_SCHEDULE = {
    "clear_old_tokens": {
        "task": "core.tasks.clear_outstanding_tokens",
        "schedule": crontab(minute=0, hour=0),
    },
    "actualize_challenges": {
        "task": "core.tasks.challenge_actualizer",
        "schedule": crontab(minute="*/15"),
    },
}


AUTH_USER_MODEL = "users.DariasUser"


AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]


REST_USE_JWT = True

ACCESS_TOKEN_LIFETIME_MINUTES = 5 * 5
REFRESH_TOKEN_LIFETIME_MINUTES = 60 * 24 * 3

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=ACCESS_TOKEN_LIFETIME_MINUTES),
    "REFRESH_TOKEN_LIFETIME": timedelta(minutes=REFRESH_TOKEN_LIFETIME_MINUTES),
    "ROTATE_REFRESH_TOKENS": False,
    "BLACKLIST_AFTER_ROTATION": True,
    "ALGORITHM": "HS256",
    "SIGNING_KEY": SECRET_KEY,
    "VERIFYING_KEY": None,
    "AUDIENCE": None,
    "ISSUER": None,
    "AUTH_HEADER_TYPES": ("Bearer",),
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "user_id",
    "AUTH_TOKEN_CLASSES": ("rest_framework_simplejwt.tokens.AccessToken",),
    "TOKEN_TYPE_CLAIM": "token_type",
    "JTI_CLAIM": "jti",
    "SLIDING_TOKEN_REFRESH_EXP_CLAIM": "refresh_exp",
    "SLIDING_TOKEN_LIFETIME": timedelta(minutes=5),
    "SLIDING_TOKEN_REFRESH_LIFETIME": timedelta(days=1),
}

DJOSER = {
    "PASSWORD_RESET_CONFIRM_URL": "#/password/reset/confirm/{uid}/{token}",
    "SEND_ACTIVATION_EMAIL": False,
    "SEND_CONFIRMATION_EMAIL": False,
    "SERIALIZERS": {
        "user": "users.serializers.AccountSerializer",
        "user_create": "users.serializers.RegistrationSerializer",
        "current_user": "users.serializers.AccountSerializer",
        "password_reset": "djoser.serializers.SendEmailResetSerializer",
        "password_reset_confirm": "djoser.serializers.PasswordResetConfirmSerializer",
    },
    "PERMISSIONS": {
        "token_create": ["rest_framework.permissions.AllowAny"],
        "interests": ["rest_framework.permissions.AllowAny"],
    },
}

# mail service
MANDRILL_API_KEY = os.environ.get("MANDRILL_API_KEY", "3iUMk-vtVCxpsh75kwdCvw")
EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
DEFAULT_FROM_EMAIL = "darya@daryasdaily.org"

# cache
REDIS_HOST = "redis"
REDIS_PORT = "6379"
PROJECT_ID = "api"
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": f"redis://{REDIS_HOST}:{REDIS_PORT}/0",
        "OPTIONS": {"CLIENT_CLASS": "django_redis.client.DefaultClient"},
        "KEY_PREFIX": PROJECT_ID,
    }
}

CELERY_BROKER_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}/1"

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True

# AWS settings
AWS_ACCESS_KEY_ID = "AKIAQUVYX6DFFUKDSGEI"
AWS_SECRET_ACCESS_KEY = "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"
AWS_S3_REGION_NAME = "us-east-2"
AWS_STORAGE_BUCKET_NAME = "dd.bucket.prod"
AWS_DEFAULT_ACL = "public-read"
AWS_S3_CUSTOM_DOMAIN = "s3.%s.amazonaws.com/%s" % (AWS_S3_REGION_NAME, AWS_STORAGE_BUCKET_NAME)
AWS_S3_OBJECT_PARAMETERS = {
    "CacheControl": "max-age=86400",
}

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "../static"),
]

AWS_STATIC_LOCATION = "%s/static" % (ENVIRONMENT_NAME)
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, AWS_STATIC_LOCATION)

AWS_MEDIA_LOCATION = "%s/media" % (ENVIRONMENT_NAME)
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, AWS_MEDIA_LOCATION)

DEFAULT_FILE_STORAGE = "storages.backends.s3boto3.S3Boto3Storage"
STATICFILES_STORAGE = "core.aws_storages.StaticStorage"

# static image path
STATIC_IMAGE_DIR = f"{BASE_DIR}/img"

AWS_HEADERS = {"Access-Control-Allow-Origin": "*"}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

if DEBUG:
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
    MEDIA_URL = "/media/"
    STATIC_ROOT = os.path.join(BASE_DIR, "static")
    STATIC_URL = "/static/"
    STATICFILES_STORAGE = "django.contrib.staticfiles.storage.StaticFilesStorage"
    MIDDLEWARE.append("sqlmiddleware.middlewares.LogSQLMiddleware")

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
MAX_FILE_SIZE = 2.0

if not DEBUG:
    import sentry_sdk
    from sentry_sdk.integrations.django import DjangoIntegration

    sentry_sdk.init(
        dsn="https://cd9ad36299b14bac9313b66e2e671cbc@o515529.ingest.sentry.io/5652275",
        integrations=[DjangoIntegration()],
        traces_sample_rate=1.0,
        send_default_pii=True,
        environment=ENVIRONMENT_NAME,
    )
