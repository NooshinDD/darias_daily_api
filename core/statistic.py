from challenges.models import Challenge, DariasUserChallenge
from rewards.models import Reward


def upgrade_lvl(user, lvl):
    user.levels.add(lvl)
    user.total_points += lvl.points
    user.save()


def give_reward(user, reward: Reward):
    user.reward_set.add(reward)
    user.total_points += reward.points
    user.save()


# method that adding point to users statistic according to challenge category
def add_points_by_category(current_challenge: DariasUserChallenge):
    field_name = "{}points".format(current_challenge.challenge.category[:-1].lower())
    for category in Challenge.CHALLENGE_CATEGORY:
        if is_challenge_by_type(category[0], current_challenge.challenge, "category"):
            update_statistic_from_challenge(current_challenge, field_name)


# method that adding point to users statistic according to challenge complexity
def add_points_by_complexity(current_challenge: DariasUserChallenge):
    field_name = "{}points".format(current_challenge.challenge.complexity[:-1].lower())
    for complexity in Challenge.CHALLENGE_COMPLEXITY:
        if is_challenge_by_type(complexity[0], current_challenge.challenge, "complexity"):
            update_statistic_from_challenge(current_challenge, field_name)


def is_challenge_by_type(name, challenge, challenge_type):
    return name == getattr(challenge, challenge_type)


def update_statistic_from_challenge(challenge, field_name):
    points = challenge.points + challenge.user.statistic.__dict__.get(field_name)
    challenge.user.statistic.__dict__.update({field_name: points})
    challenge.user.statistic.save()
