from datetime import datetime

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken

from users.models import DariasUser, Statistic

from challenges.models import Challenge, DariasUserChallenge
from rewards.models import Reward

from .celery import app as celery_app
from .date_functions import end_of_the_day
from .statistic import give_reward


@celery_app.task
def send_email_with_password(new_password: str, to: list):
    msg = EmailMultiAlternatives(
        subject="Darya's Daily: Forgot Password ",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=to,
        headers={
            "Reply-To": "Service <{}>".format(settings.DEFAULT_FROM_EMAIL)
        },  # optional extra headers
    )
    rendered = render_to_string("restore_password.html", {"new_password": new_password})
    msg.attach_alternative(
        rendered,
        "text/html",
    )
    return msg.send()


@celery_app.task
def clear_outstanding_tokens():
    OutstandingToken.objects.all().delete()


@celery_app.task
def challenge_actualizer():
    challenges_with_date = Challenge.objects.filter(date__gt=timezone.now().date())
    for challenge in challenges_with_date:
        update_challenge_list(challenge.id)


@celery_app.task
def update_challenge_list(challenge_id):
    challenge = Challenge.objects.get(pk=challenge_id)
    date = datetime.combine(challenge.date, datetime.max.time())
    DariasUserChallenge.objects.filter(
        end_date=end_of_the_day(date),
        challenge__complexity=challenge.complexity,
        challenge__category=challenge.category,
        user__users_labels__in=challenge.labels.all(),
    ).update(challenge=challenge)


@celery_app.task
def update_users_statistic(user_id):
    user = DariasUser.objects.get(pk=user_id)
    created, _ = Statistic.objects.get_or_create(user=user)
    rewards = (
        Reward.objects.filter(condition_name=Reward.LOGIN)
        .exclude(user=user)
        .order_by("condition_amount")
    )
    if last_login_date := user.last_login.date():
        last_login_condition_days = (timezone.now().date() - last_login_date).days
        login_streak_condition_days = (last_login_date - user.statistic.login_streak_update).days
        if created and last_login_condition_days - login_streak_condition_days == 1:
            user.statistic.login_streak += 1
            user.statistic.login_streak_update = timezone.now().date()
        elif login_streak_condition_days != last_login_condition_days:
            user.statistic.login_streak = 1
            user.statistic.login_streak_update = timezone.now().date()
        user.statistic.save()
    user.last_login = timezone.now()
    user.save(update_fields=["last_login"])
    if rewards and [
        i.condition_amount for i in rewards if user.statistic.login_streak >= i.condition_amount
    ]:
        reward = rewards.first()
        give_reward(user, reward)
