from django.contrib import admin
from django.db import models
from django.forms import Textarea
from django.utils.safestring import mark_safe

from rewards.models import Reward


@admin.register(Reward)
class RewardAdmin(admin.ModelAdmin):
    fields = (
        "title",
        "points",
        "description",
        "photo",
        "photo_tag",
        "condition_name",
        "condition_category",
        "condition_amount",
    )
    list_display = ("title", "condition_name", "points", "created_at")
    readonly_fields = ("photo_tag",)
    actions = ("delete_queryset",)

    formfield_overrides = {
        models.CharField: {"widget": Textarea(attrs={"rows": 2, "cols": 25})},
    }

    def photo_tag(self, obj):
        photo = obj.photo
        return mark_safe('<img src="%s" width="400" height="400" />\n' % photo.url)

    def delete_queryset(self, request, queryset):
        return queryset.delete()

    def has_delete_permission(self, request, obj=None):
        return False

    photo_tag.short_description = "Uploaded photo"
    delete_queryset.short_description = "Delete picked"
