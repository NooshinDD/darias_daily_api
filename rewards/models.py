from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from challenges.mixins import AutoLogMixin
from challenges.models import Challenge
from core.aws_storages import MediaStorage
from validators import validate_image


def reward_photo_file_name(instance, filename):
    return "/".join(["content", "rewards", instance.title, filename])


class Reward(AutoLogMixin):
    COMPLEXITY = "COMPLEXITY"
    CATEGORY = "CATEGORY"
    LOGIN = "LOGIN"
    RATE = "RATE"

    CONDITION_NAMES = (
        (COMPLEXITY, "Level"),
        (CATEGORY, "Category"),
        (LOGIN, "Login to the application"),
        (RATE, "Successful rate"),
    )

    title = models.CharField(max_length=30, null=False, help_text="Name of the badge", unique=True)
    description = models.CharField(
        max_length=512, default="", help_text="Description text under badge"
    )
    points = models.IntegerField(
        default=0, help_text="Total amount of point that user will receive after achieving a badge"
    )
    photo = models.ImageField(
        (_("photo")),
        null=True,
        default=None,
        upload_to=reward_photo_file_name,
        validators=[validate_image],
        help_text="Please, upload round image, max file size is %sMB" % str(settings.MAX_FILE_SIZE),
        storage=MediaStorage(),
    )
    condition_name = models.CharField(
        max_length=25,
        choices=CONDITION_NAMES,
        default="",
        help_text="The name of the condition by fulfilling which the user can receive a badge, "
        f"for {CONDITION_NAMES[0][1]} and {CONDITION_NAMES[1][1]}, you have to pick"
        " a corresponding condition category, for rest condition "
        "names category will not be affected",
    )
    condition_category = models.CharField(
        max_length=25,
        choices=Challenge.CHALLENGE_COMPLEXITY + Challenge.CHALLENGE_CATEGORY,
        default="",
        help_text=f"Makes effect only with {CONDITION_NAMES[0][1]} "
        f"and {CONDITION_NAMES[1][1]} Condition names",
    )
    condition_amount = models.IntegerField(
        (_("number")),
        default=1,
        help_text=f"For Condition names: {CONDITION_NAMES[0][1]}, "
        f"{CONDITION_NAMES[1][1]}, {CONDITION_NAMES[3][1]} \n"
        "Number of points that user has to reach to achieve a badge,\n"
        f"for Condition name {CONDITION_NAMES[2][1]} - amount of days",
    )

    user = models.ManyToManyField("users.DariasUser", related_query_name="rewards")

    @property
    def photo_url(self):
        if self.photo:
            return self.photo.url

    def clean(self):
        if self.points < 0:
            raise ValidationError("Points should be not less than 0")
        if self.condition_amount <= 0:
            raise ValidationError("Number should be more than 0")
        if (
            self.condition_category not in [i[0] for i in Challenge.CHALLENGE_COMPLEXITY]
            and self.condition_name == Reward.COMPLEXITY
        ):
            raise ValidationError("wrong condition name or category")

        if (
            self.condition_category not in [i[0] for i in Challenge.CHALLENGE_CATEGORY]
            and self.condition_name == Reward.CATEGORY
        ):
            raise ValidationError("wrong condition name or category")
