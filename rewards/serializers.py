from django.utils import timezone
from rest_framework import serializers

from users.models import Statistic

from challenges.models import DariasUserChallenge
from core.date_functions import days_back
from rewards.models import Reward


class StatisticSerializer(serializers.ModelSerializer):
    food_impact_points = serializers.DecimalField(decimal_places=0, max_digits=10)
    energy_points = serializers.DecimalField(decimal_places=0, max_digits=10)
    plants_animals_points = serializers.DecimalField(decimal_places=0, max_digits=10)
    water_points = serializers.DecimalField(decimal_places=0, max_digits=10)
    week_score = serializers.SerializerMethodField("get_week_score")
    completed_tasks = serializers.SerializerMethodField("get_completed_tasks")

    class Meta:
        model = Statistic
        fields = (
            "food_impact_points",
            "energy_points",
            "plants_animals_points",
            "water_points",
            "week_score",
            "completed_tasks",
        )

    def get_week_score(self, obj):
        return sum(
            i.points
            for i in obj.user.challenges.filter(start_date__range=[days_back(7), timezone.now()])
        )

    def get_completed_tasks(self, obj):
        return obj.user.challenges.filter(status=DariasUserChallenge.APPROVED_STATUS).count()


class RewardSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    title = serializers.CharField(required=False)

    class Meta:
        model = Reward
        fields = (
            "id",
            "title",
            "description",
            "points",
            "photo",
            "condition_name",
            "condition_category",
            "condition_amount",
        )
