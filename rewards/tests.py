from django.test import Client, TestCase
from django.utils import timezone

from users.models import DariasUser, Statistic

from challenges.models import Challenge
from rewards.models import Reward


class RewardBase(TestCase):
    email = "testuser@example.com"
    password = "Password1234!"
    rewards_data = {
        "title": "test_rewards Title",
        "description": "description",
        "points": 100,
        "photo": "",
        "condition_name": Reward.CATEGORY,
        "condition_category": Challenge.WATER_CATEGORY,
        "condition_amount": 5,
    }

    def setUp(self):
        self.client = Client()
        self.user = DariasUser.objects.create_user(self.email, password=self.password)
        self.user.first_name = "John"
        self.user.last_name = "Doe"
        self.user.birthday_date = "2021-02-02"
        self.user.statistic = Statistic.objects.create(
            user=self.user, login_streak_update=timezone.now().date()
        )
        self.reward = Reward.objects.create(**self.rewards_data)

    def _login(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + resp.json()["access"]
        self.assertEqual(200, resp.status_code)

    def test_rewards_list(self):
        self._login()
        resp = self.client.get("/rewards/")
        self.assertEqual(200, resp.status_code)
        self.assertIn("statistic", resp.json())
        self.assertEqual(self.rewards_data["title"], resp.json()["badges"][0]["title"])
        self.assertEqual(resp.json()["badges"][0]["is_reached"], False)

    def test_rewards_is_reached(self):
        self._login()
        self.user.reward_set.add(self.reward)
        resp = self.client.get("/rewards/")
        self.assertEqual(200, resp.status_code)
        self.assertIn("statistic", resp.json())
        self.assertEqual(resp.json()["badges"][0]["is_reached"], True)
