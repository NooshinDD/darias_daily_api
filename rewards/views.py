from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rewards.models import Reward
from rewards.serializers import RewardSerializer, StatisticSerializer


class RewardsViewSet(viewsets.GenericViewSet):

    http_method_names = ("post", "get")
    queryset = Reward.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = RewardSerializer

    def list(self, request, *args, **kwargs):
        """gives list rewards"""
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        union_ids = [i.id for i in self.queryset & self.queryset.filter(user=request.user)]

        def _is_reached(reward):
            return bool(reward["id"] in union_ids)

        [i.update({"is_reached": _is_reached(i)}) for i in serializer.data]
        statistic_serializer = StatisticSerializer(instance=request.user.statistic)
        new_serializer_data = {
            "badges": serializer.data,
            "statistic": statistic_serializer.data,
        }
        return Response(new_serializer_data)
