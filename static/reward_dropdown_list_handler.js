const COMPLEXITY = 'COMPLEXITY';
const CATEGORY = 'CATEGORY';

const COMPLEXITY_LIST = {
  EASY_C: 'Easy',
  MEDIUM_C: 'Medium',
  HARD_C: 'Hard',
};

const CATEGORY_LIST = {
  FOOD_IMPACT_C: 'Food impact',
  PLANTS_ANIMALS_C: 'Plants&Animals',
  ENERGY_C: 'Energy',
  WATER_C: 'Water',
};

function changeOptions(list, elem) {
  elem.empty();
  django.jQuery.each(list, function (key, value) {
    elem.append(
      django.jQuery('<option></option>').attr('value', key).text(value),
    );
  });
}

function renderElements() {
  const selectedName = django
    .jQuery('#id_condition_name')
    .find(':selected')
    .val();
  const elemCategorySelect = django.jQuery('#id_condition_category');

  const elemCategory = django.jQuery('.form-row.field-condition_category');

  if ([COMPLEXITY, CATEGORY].includes(selectedName)) {
    elemCategory.show();

    changeOptions(
      COMPLEXITY === selectedName ? COMPLEXITY_LIST : CATEGORY_LIST,
      elemCategorySelect,
    );
  } else {
    elemCategory.hide();
  }
}

django.jQuery(document).ready(function () {
  renderElements();
});

django.jQuery(document).change(function () {
  renderElements();
});