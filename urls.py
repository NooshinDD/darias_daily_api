from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework import permissions

from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from users.views import GoogleOAuth2View, TokenPairView

from core.settings import DEBUG

schema_view = get_schema_view(
    openapi.Info(
        title="Daria`s Daily API",
        default_version="v1",
        description="",
    ),
    url=f"https://{settings.ENVIRONMENT_NAME}.daryasdaily.org/" if not DEBUG else None,
    public=True,
    permission_classes=(permissions.AllowAny,),
)
urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "docs/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("users/", include("users.urls")),
    path("auth/jwt/create/", TokenPairView.as_view(), name="token_obtain_pair"),
    path("auth/", include("djoser.urls.jwt")),
    path("challenges/", include("challenges.urls")),
    path("rewards/", include("rewards.urls")),
    path("auth/google/", GoogleOAuth2View.as_view()),
    path("auth/", include("djoser.social.urls")),
]


if settings.DEBUG:
    urlpatterns += [
        path("__sql/", include("sqlmiddleware.urls")),
    ]
