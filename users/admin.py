import datetime

from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.sites.models import Site
from django.db import models
from django.forms import Textarea
from django.urls import reverse
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.token_blacklist.models import BlacklistedToken, OutstandingToken

from users.models import DariasUser, Interest, Level, Streak

from challenges.models import ChallengeApproval, DariasUserChallenge


class UserChallengeInline(admin.TabularInline):
    model = DariasUserChallenge
    fields = ("challenge", "points", "status", "approval_created_at")
    readonly_fields = ("points", "status", "challenge", "approval_created_at")
    ordering = ("challenge_approval__created_at",)

    def approval_created_at(self, obj):
        address_id = obj.challenge_approval.pk
        info = (ChallengeApproval._meta.app_label, ChallengeApproval._meta.model_name)
        url = reverse("admin:{}_{}_change".format(*info), args=(address_id,))
        formatted_date = obj.challenge_approval.created_at.strftime("%m/%d/%Y, %H:%M:%S")

        return format_html('<a href="{url}">{text}</a>'.format(url=url, text=formatted_date))

    def challenge(self, obj: DariasUserChallenge):
        return obj.challenge_approval

    def get_queryset(self, request):
        qs = super(UserChallengeInline, self).get_queryset(request)
        return qs.filter(
            status__in=[
                DariasUserChallenge.APPROVED_STATUS,
                DariasUserChallenge.REJECTED_STATUS,
                DariasUserChallenge.PENDING_STATUS,
            ]
        )

    def has_add_permission(self, request, obj):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(DariasUser)
class UserAdmin(admin.ModelAdmin):
    inlines = [UserChallengeInline]
    readonly_fields = [
        "image_tag",
        "challenges",
        "total_points",
        "week_progress",
        "multiplier",
        "level",
    ]
    list_display = ("first_name", "email", "date_joined")
    actions = ("delete_queryset",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "photo",
                    "image_tag",
                )
            },
        ),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "date_joined",
                    "birthday_date",
                    "city",
                    "biography",
                )
            },
        ),
        (
            _("Progress"),
            {"fields": ("total_points", "multiplier", "level")},
        ),
        (_("Statistic"), {"fields": ("challenges", "week_progress")}),
    )
    formfield_overrides = {
        models.CharField: {"widget": Textarea(attrs={"rows": 2, "cols": 25})},
    }

    def image_tag(self, obj: DariasUser):
        return format_html('<img src="{}" width="auto" height="200px" />'.format(obj.photo.url))

    def challenges(self, obj: DariasUser):
        return len(obj.challenges.filter(status=DariasUserChallenge.APPROVED_STATUS))

    def week_progress(self, obj: DariasUser):
        today = datetime.datetime.utcnow()
        start = (today - datetime.timedelta(days=3, hours=12)).strftime("%Y-%m-%dT%H:%M:%SZ")
        end = (today + datetime.timedelta(days=3, hours=12)).strftime("%Y-%m-%dT%H:%M:%SZ")

        approved_challenges = obj.challenges.filter(
            status=DariasUserChallenge.APPROVED_STATUS, start_date__range=[start, end]
        )
        return sum(i.challenge.score for i in approved_challenges)

    def multiplier(self, obj: DariasUser):
        multiplier = obj.streaks.all().last()
        if not multiplier:
            return 1
        else:
            return multiplier.multiplier

    def level(self, obj):
        lvl = obj.levels.all().last()
        if not lvl:
            return "starter"
        return lvl.name

    def delete_queryset(self, request, queryset):
        return queryset.delete()

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    delete_queryset.short_description = "Delete selected"
    image_tag.short_description = "Avatar"
    challenges.short_description = "Completed tasks"


@admin.register(Interest)
class InterestAdmin(admin.ModelAdmin):
    pass


@admin.register(Streak)
class StreakAdmin(admin.ModelAdmin):
    model = Streak
    fields = ("multiplier", "days")
    list_display = ("multiplier", "days")

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    model = Level
    readonly_fields = ("image_tag",)
    list_display = ("name", "score")
    fieldsets = (
        (
            "Level",
            {"fields": ("name", "score")},
        ),
        (
            _("Achievement"),
            {"fields": ("description", "points", "photo", "image_tag")},
        ),
    )

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def image_tag(self, obj: Level):
        return format_html('<img src="{}" width="auto" height="200px" />'.format(obj.photo.url))

    image_tag.short_description = "Uploaded achievement picture"


admin.site.unregister(Interest)
admin.site.unregister(OutstandingToken)
admin.site.unregister(BlacklistedToken)
admin.site.unregister(Group)
admin.site.unregister(Site)
