# Generated by Django 3.1.7 on 2021-03-29 12:18

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("users", "0011_rename_complexity_fields"),
    ]

    operations = [
        migrations.CreateModel(
            name="Level",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
                    ),
                ),
                ("name", models.CharField(default="", max_length=64)),
                ("score", models.IntegerField(default=0)),
                (
                    "user",
                    models.ManyToManyField(related_name="levels", to=settings.AUTH_USER_MODEL),
                ),
            ],
        ),
    ]
