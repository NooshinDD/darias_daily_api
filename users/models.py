import uuid

from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken

from django_lifecycle import AFTER_UPDATE, LifecycleModel, LifecycleModelMixin, hook

from challenges.mixins import AutoLogMixin
from challenges.models import Challenge
from core.aws_storages import MediaStorage
from core.statistic import give_reward, upgrade_lvl
from rewards.models import Reward
from validators import validate_image


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given email, and password.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(email, password, **extra_fields)


def user_photo_file_name(instance, filename):
    return "/".join(["content", "users", instance.email, filename])


class DariasUser(LifecycleModel, AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(_("email address"), unique=True)
    first_name = models.CharField(_("first name"), max_length=30)
    last_name = models.CharField(_("last name"), max_length=30)
    birthday_date = models.DateField(null=True, blank=True)
    biography = models.CharField(max_length=512, blank=True, default="")
    city = models.CharField(max_length=512, blank=True, null=True)
    auth_provider = models.CharField(max_length=255, blank=True, null=True)
    photo = models.ImageField(
        (_("photo")),
        null=True,
        default=None,
        upload_to=user_photo_file_name,
        validators=[validate_image],
        help_text="Max file size is %sMB" % str(settings.MAX_FILE_SIZE),
        storage=MediaStorage(),
    )
    total_points = models.DecimalField(default=0, decimal_places=2, max_digits=10)
    streak_days = models.IntegerField(default=0)

    @hook(AFTER_UPDATE, when="total_points", has_changed=True)
    def check_reward(self):
        reward = (
            Reward.objects.filter(condition_name=Reward.RATE)
            .exclude(user=self)
            .order_by("condition_amount")
            .first()
        )
        if reward and reward.condition_amount <= self.total_points:
            give_reward(self, reward)

    @hook(AFTER_UPDATE, when="total_points", has_changed=True)
    def check_lvl(self):
        level = Level.objects.all().exclude(user=self).order_by("score").first()
        if level and level.score <= self.total_points:
            upgrade_lvl(self, level)

    username = None
    objects = UserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta(AbstractUser.Meta):
        swappable = "AUTH_USER_MODEL"
        db_table = "auth_user"

    def __str__(self):
        return self.first_name

    @property
    def photo_url(self):
        if self.photo:
            return self.photo.url

    @property
    def current_streak(self):
        if streaks := self.streaks.all():
            return streaks.last()

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {"refresh": str(refresh), "access": str(refresh.access_token)}


class Interest(models.Model):
    user = models.ManyToManyField("DariasUser", related_name="interests")
    interest = models.CharField(max_length=30)
    is_active = models.BooleanField(default=True)


class Streak(models.Model):
    multiplier = models.DecimalField(default=0, decimal_places=1, max_digits=3)
    days = models.IntegerField(default=0)
    user = models.ManyToManyField("DariasUser", related_name="streaks")


class Statistic(LifecycleModelMixin, models.Model):
    user = models.OneToOneField("DariasUser", on_delete=models.CASCADE)
    login_streak = models.SmallIntegerField(default=1)
    login_streak_update = models.DateField(default=timezone.now)
    easy_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)
    medium_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)
    hard_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)
    food_impact_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)
    energy_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)
    plants_animals_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)
    water_points = models.DecimalField(default=0, decimal_places=3, max_digits=10)

    @hook(AFTER_UPDATE, when_any=["easy_points", "medium_points", "hard_points"])
    def rewards_by_complexity(self):
        user = self.user
        rewards = Reward.objects.filter(condition_name=Reward.COMPLEXITY).exclude(user=user)
        if rewards:
            for reward in rewards.filter(condition_category=Challenge.EASY_COMPLEXITY):
                if reward.condition_amount <= self.easy_points:
                    give_reward(user, reward)
            for reward in rewards.filter(condition_category=Challenge.MEDIUM_COMPLEXITY):
                if reward.condition_amount <= self.medium_points:
                    give_reward(user, reward)
            for reward in rewards.filter(condition_category=Challenge.HARD_COMPLEXITY):
                if reward.condition_amount <= self.hard_points:
                    give_reward(user, reward)

    @hook(
        AFTER_UPDATE,
        when_any=["food_impact_points", "energy_points", "plants_animals_points", "water_points"],
    )
    def rewards_by_category(self):
        user = self.user
        rewards = Reward.objects.filter(condition_name=Reward.CATEGORY).exclude(user=user)
        if rewards:
            for reward in rewards.filter(condition_category=Challenge.FOOD_IMPACT_CATEGORY):
                if reward.condition_amount <= self.food_impact_points:
                    give_reward(user, reward)
            for reward in rewards.filter(condition_category=Challenge.ENERGY_CATEGORY):
                if reward.condition_amount <= self.energy_points:
                    give_reward(user, reward)
            for reward in rewards.filter(condition_category=Challenge.PLANTS_ANIMALS_CATEGORY):
                if reward.condition_amount <= self.plants_animals_points:
                    give_reward(user, reward)
            for reward in rewards.filter(condition_category=Challenge.WATER_CATEGORY):
                if reward.condition_amount <= self.water_points:
                    give_reward(user, reward)


def level_photo_file_name(instance, filename):
    return "/".join(["content", "levels", instance.name, filename])


class Level(AutoLogMixin):
    name = models.CharField(default="", max_length=64)
    score = models.IntegerField(default=0, help_text="Amount of point to reach the level")
    user = models.ManyToManyField("DariasUser", related_name="levels")
    description = models.CharField(
        max_length=512, default="", help_text="Description of the achievement"
    )
    points = models.IntegerField(
        default=0, help_text="Amount of points that the user will receive upon reaching the level"
    )
    photo = models.ImageField(
        (_("photo")),
        null=True,
        default=None,
        upload_to=level_photo_file_name,
        validators=[validate_image],
        help_text="Max file size is %sMB" % str(settings.MAX_FILE_SIZE),
        storage=MediaStorage(),
    )

    class Meta:
        verbose_name = "Achievement"
