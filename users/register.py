from django.contrib.auth import authenticate
from rest_framework.exceptions import AuthenticationFailed

from core import settings

from .models import DariasUser


def register_social_user(user_data):
    provider = "google" if not user_data.get("provider") else user_data["provider"]
    if filtered_user_by_email := DariasUser.objects.filter(email=user_data["email"]):
        if provider != filtered_user_by_email[0].auth_provider:
            raise AuthenticationFailed(
                detail="Please continue your login using " + filtered_user_by_email[0].auth_provider
            )
        registered_user = authenticate(
            email=user_data["email"], password=settings.SOCIAL_GOOGLE_CLIENT_TEMP_PASSWORD
        )
        return {
            "first_name": registered_user.first_name,
            "last_name": registered_user.last_name,
            "email": registered_user.email,
            "tokens": registered_user.tokens(),
        }
    else:
        user = {
            "first_name": user_data["given_name"],
            "last_name": user_data["family_name"],
            "email": user_data["email"],
            "password": settings.SOCIAL_GOOGLE_CLIENT_TEMP_PASSWORD,
        }
        user = DariasUser.objects.create_user(**user)
        user.auth_provider = provider
        user.save()
        new_user = authenticate(
            email=user_data["email"], password=settings.SOCIAL_GOOGLE_CLIENT_TEMP_PASSWORD
        )
        return {
            "email": new_user.email,
            "first_name": new_user.first_name,
            "last_name": new_user.last_name,
            "tokens": new_user.tokens(),
        }
