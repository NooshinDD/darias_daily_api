import os
from decimal import Decimal

from django.conf import settings
from django.contrib.auth.models import update_last_login
from django.core.files import File
from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from users import google
from users.models import DariasUser, Interest, Level, Statistic, Streak
from users.register import register_social_user

from challenges.models import Challenge, DariasUserChallenge
from challenges.serializers import ChallengeLabelSerializer
from core.statistic import upgrade_lvl


class InterestSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    interest = serializers.CharField(required=False)

    class Meta:
        model = Interest
        fields = ("id", "interest")


class InterestListSerializer(serializers.Serializer):
    interests = serializers.ListSerializer(child=InterestSerializer())

    class Meta:
        fields = ("id",)

    def update(self, instance, validated_data):
        instance.interests.clear()
        if self.validated_data["interests"]:
            interest_ids = [i.get("id") for i in validated_data["interests"]]
            interests = Interest.objects.filter(pk__in=interest_ids)
            for item in interests:
                instance.interests.add(item)
            return instance


class UserProgressSerializer(serializers.Serializer):
    food_impact = serializers.SerializerMethodField("get_food_progress")
    plants_animals = serializers.SerializerMethodField("get_plants_animals_progress")
    water = serializers.SerializerMethodField("get_water_progress")
    energy = serializers.SerializerMethodField("get_energy_progress")

    class Meta:
        fields = ("food_impact", "plants_animals", "water", "energy")

    def get_food_progress(self, queryset):
        challenges = queryset.filter(challenge__category=Challenge.FOOD_IMPACT_CATEGORY)
        if challenges:
            return len(challenges) / len(queryset) * 100

    def get_plants_animals_progress(self, queryset):
        challenges = queryset.filter(challenge__category=Challenge.PLANTS_ANIMALS_CATEGORY)
        if challenges:
            return len(challenges) / len(queryset) * 100

    def get_water_progress(self, queryset):
        challenges = queryset.filter(challenge__category=Challenge.WATER_CATEGORY)
        if challenges:
            return len(challenges) / len(queryset) * 100

    def get_energy_progress(self, queryset):
        challenges = queryset.filter(challenge__category=Challenge.ENERGY_CATEGORY)
        if challenges:
            return len(challenges) / len(queryset) * 100


class LevelSerializer(serializers.ModelSerializer):
    is_reached = serializers.SerializerMethodField("get_status")

    class Meta:
        model = Level
        fields = ("name", "score", "description", "points", "photo", "is_reached")

    def get_status(self, obj: Level):
        if self.context.get("request"):
            return obj in self.context["request"].user.levels.all()
        return False


class UserLevelSerializer(serializers.ModelSerializer):
    current_level = serializers.SerializerMethodField("get_level")
    level_progress = serializers.SerializerMethodField("get_level_progress")

    class Meta:
        model = DariasUser
        fields = ("current_level", "level_progress")

    def get_level(self, obj: DariasUser):
        levels = obj.levels.all()
        if not levels:
            level = Level.objects.exclude(user=obj).order_by("score").first()
            if level and level.score <= obj.total_points:
                upgrade_lvl(obj, level)
                return level.name
            levels = Level.objects.all().order_by("-score")
        return levels.last().name

    def get_level_progress(self, obj: DariasUser):
        next_level = Level.objects.exclude(user=obj).order_by("score").first()
        if not next_level:
            return 100
        current_level = obj.levels.all().last()
        if not current_level:
            return 100 * obj.total_points / next_level.score
        return (
            100
            * (obj.total_points - current_level.score)
            / (next_level.score - current_level.score)
        )


class AccountSerializer(serializers.ModelSerializer):
    interests = InterestSerializer(many=True, read_only=True)
    users_labels = ChallengeLabelSerializer(many=True, read_only=True)
    email = serializers.EmailField(required=True)
    challenge_progress = serializers.SerializerMethodField("get_progress")
    level_info = serializers.SerializerMethodField("get_user_level")
    available_achievements = serializers.SerializerMethodField("get_achievement")
    current_multiplier = serializers.SerializerMethodField("get_streak")
    total_points = serializers.DecimalField(max_digits=10, decimal_places=0, read_only=True)

    class Meta:
        model = DariasUser
        fields = (
            "id",
            "first_name",
            "last_name",
            "birthday_date",
            "biography",
            "city",
            "email",
            "photo",
            "interests",
            "users_labels",
            "total_points",
            "challenge_progress",
            "level_info",
            "current_multiplier",
            "available_achievements",
        )

    def update(self, instance: DariasUser, validated_data):
        instance.first_name = validated_data.get("first_name")
        instance.last_name = validated_data.get("last_name")
        instance.birthday_date = validated_data.get("birthday_date")
        instance.biography = validated_data.get("biography")
        instance.city = validated_data.get("city")
        instance.email = validated_data.get("email").lower()
        instance.save()
        return instance

    def get_progress(self, obj: DariasUser):
        completed_challenges = obj.challenges.filter(status=DariasUserChallenge.APPROVED_STATUS)
        return UserProgressSerializer(completed_challenges).data

    def get_user_level(self, obj: DariasUser):
        return UserLevelSerializer(obj).data

    def get_streak(self, obj: DariasUser):
        if not obj.current_streak:
            return Decimal(1.0)
        return obj.current_streak.multiplier

    def get_achievement(self, obj: DariasUser):
        return LevelSerializer(
            Level.objects.all().order_by("score"), many=True, context=self.context
        ).data


class RegistrationSerializer(AccountSerializer):
    class Meta(AccountSerializer.Meta):
        fields = AccountSerializer.Meta.fields + ("password",)

    read_only_fields = (
        "id",
        "password",
    )

    def create(self, validated_data):
        validated_data["email"] = validated_data.get("email").lower()
        user = DariasUser.objects.create_user(**validated_data)
        Statistic.objects.create(user=user)
        return user

    def validate(self, data):
        """
        Check that email exists
        """
        if DariasUser.objects.filter(email=data["email"]).exists():
            raise serializers.ValidationError("Wrong image name")
        return data


class AvatarSerializer(serializers.ModelSerializer):
    photo = serializers.ImageField()

    class Meta:
        model = DariasUser
        fields = ["photo"]

    def save(self, *args, **kwargs):
        if self.instance.photo:
            self.instance.photo.delete()
        return super().save(**kwargs)


class StaticAvatarSerializer(serializers.Serializer):
    photo = serializers.CharField()

    class Meta:
        fields = ["photo"]

    def save(self, **kwargs):
        if self.instance.photo:
            self.instance.photo.delete()
        image_path = f"{settings.STATIC_IMAGE_DIR}/{self.validated_data['photo']}.png"
        file = File(open(image_path, "rb"))
        file.name = self.validated_data["photo"] + ".png"
        self.instance.photo = file
        self.instance.save()

    def validate(self, data):
        """
        Check that name of avatar in static images
        """
        image_folder_path = f"{settings.STATIC_IMAGE_DIR}/"
        image_names = []
        for file in os.listdir(image_folder_path):
            image_names.append(os.path.join(file).split(".", 1)[0])
        if data["photo"] not in image_names:
            raise serializers.ValidationError("Wrong image name")
        return data


class CustomTokenPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        attrs["email"] = attrs["email"].lower()
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data["refresh"] = str(refresh)
        data["access"] = str(refresh.access_token)
        if not self.user.last_login:
            update_last_login(None, self.user)
        return data


class AccountExistenceSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class StreakSerializer(serializers.ModelSerializer):
    current_multiplier = serializers.SerializerMethodField("get_streak")
    days_complete = serializers.SerializerMethodField("get_completed_days")
    streak_days = serializers.SerializerMethodField("get_next_streak_days")

    class Meta:
        model = DariasUser
        fields = ("current_multiplier", "days_complete", "streak_days")

    def get_streak(self, obj):
        if not obj.current_streak:
            return Decimal(1.0)
        return obj.current_streak.multiplier

    def get_next_streak_days(self, obj):
        streak = Streak.objects.exclude(user=obj).order_by("days").first()
        if not obj.current_streak:
            return streak.days
        return streak.days - obj.current_streak.days

    def get_completed_days(self, obj):
        if not obj.current_streak:
            return obj.streak_days
        return obj.streak_days - obj.current_streak.days


class GoogleSocialAuthSerializer(serializers.Serializer):
    auth_token = serializers.CharField()

    def validate_auth_token(self, auth_token):
        user_data = google.Google.validate(auth_token)
        try:
            user_data["sub"]
        except:
            raise serializers.ValidationError(
                "The token is invalid or expired. Please login again."
            )
        if user_data["aud"] != settings.SOCIAL_GOOGLE_CLIENT_ID:
            raise AuthenticationFailed("oops, who are you?")
        return register_social_user(user_data)
