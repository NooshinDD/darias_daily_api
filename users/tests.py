import json
import os

from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase

from PIL import Image
from six import BytesIO

from users.models import DariasUser, Level, Streak
from users.serializers import AvatarSerializer


class UserRegistrationTestCase(TestCase):

    extra_fields = [
        "photo_url",
        "interests",
        "users_labels",
        "total_points",
        "challenge_progress",
        "level_info",
        "current_multiplier",
        "available_achievements",
    ]

    email = "Testuser@example.com"
    password = "Password1234!"

    def test_login_before_registration(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(401, resp.status_code)

    def test_user_registration(self):
        # Register
        register_resp_data = {
            "first_name": "Test",
            "last_name": "Testov",
            "birthday_date": "2021-02-11",
            "biography": "string",
            "city": "string",
            "password": self.password,
            "email": self.email,
        }
        self.level = Level.objects.create(name="starter", score=0)
        resp = self.client.post("/users/", register_resp_data)
        self.assertEquals(200, resp.status_code)
        self.user = DariasUser.objects.get(email=self.email.lower())
        self.assertIsNotNone(self.user)
        self.client = Client()
        auth_data = {"email": self.email.lower(), "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(200, resp.status_code)
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + resp.json()["access"]
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(len(resp.json()), len(register_resp_data) + len(self.extra_fields))


class UserProfileTestCase(TestCase):
    email = "testuser@example.com"
    password = "Password1234!"
    data = {}

    def setUp(self):
        self.level = Level.objects.create(name="starter", score=0)
        self.user = DariasUser.objects.create_user(self.email, password=self.password)
        self.user.first_name = "John"
        self.user.last_name = "Doe"
        self.user.birthday_date = "2021-02-11"
        self.user.save()

    def _login(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(200, resp.status_code)
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + resp.json()["access"]

    def test_auth_with_token(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(200, resp.status_code)
        self.assertIn("access", resp.json().keys())

    def test_auth_failed(self):
        auth_data = {"email": "Wrongemail@mail.com", "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(401, resp.status_code)

    def test_get_current_user_profile(self):
        self._login()
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(str(self.user.id), resp.json()["id"])

    def test_edit_current_user_profile(self):
        self._login()
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        edit_resp_data = resp.json()
        edit_resp_data["first_name"] = "Edited"
        edit_resp_data["city"] = "Palo Alto, CA"
        edit_resp_data["biography"] = "red"
        edit_resp_data["last_name"] = "Default Calendar"
        edit_resp_data.pop("interests")
        resp = self.client.patch(
            "/users/me/", json.dumps(edit_resp_data), content_type="application/json"
        )
        self.assertEquals(200, resp.status_code)
        self.assertIn("challenge_progress", resp.json())
        user = DariasUser.objects.get(id=self.user.id)
        check_fields = ["first_name", "city", "biography", "last_name"]
        for field in check_fields:
            self.assertEquals(getattr(user, field), edit_resp_data[field])

    def test_upload_avatar(self):
        image = BytesIO()
        Image.new("RGB", (100, 100)).save(image, "JPEG")
        image.seek(0)

        self.data["photo"] = SimpleUploadedFile("image.jpg", image.getvalue())
        serializer = AvatarSerializer(data=self.data)
        self.assertEqual(serializer.is_valid(), True)

    def test_upload_static_avatar(self):
        self._login()
        image_folder_path = f"{settings.STATIC_IMAGE_DIR}/"
        image_names = []
        for file in os.listdir(image_folder_path):
            image_names.append(os.path.join(file).split(".", 1)[0])
        photo_name = image_names[1]
        resp = self.client.post(
            "/users/static_avatar/", data={"photo": photo_name}, content_type="application/json"
        )
        self.assertEquals(200, resp.status_code)
        user = DariasUser.objects.get(id=self.user.id)
        self.assertEquals(f"content/users/testuser@example.com/{photo_name}.png", user.photo)
        user.photo.delete()

    def test_set_password(self):
        self._login()
        set_password_data = {"new_password": "string123", "current_password": self.password}
        resp = self.client.post(
            "/users/set_password/", data=set_password_data, content_type="application/json"
        )
        self.assertEquals(204, resp.status_code)
        old_auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", old_auth_data)
        self.assertEquals(401, resp.status_code)
        new_auth_data = {"email": self.email, "password": set_password_data["new_password"]}
        resp = self.client.post("/auth/jwt/create/", new_auth_data)
        self.assertEquals(200, resp.status_code)
        self.assertIn("access", resp.json().keys())

    def test_set_password_invalid_data(self):
        self._login()
        set_password_data = {"new_password": "string", "current_password": self.password}
        resp = self.client.post(
            "/users/set_password/", data=set_password_data, content_type="application/json"
        )
        self.assertEquals(400, resp.status_code)
        old_auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", old_auth_data)
        self.assertEquals(200, resp.status_code)
        self.assertIn("access", resp.json().keys())


class UserStreakTestCase(TestCase):
    email = "testuser@example.com"
    password = "Password1234!"

    def setUp(self):
        self.user = DariasUser.objects.create_user(self.email, password=self.password)
        self.user.first_name = "John"
        self.user.last_name = "Doe"
        self.user.birthday_date = "2021-02-11"
        self.streak = Streak.objects.create(multiplier=1.5, days=2)
        self.user.save()

    def _login(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(200, resp.status_code)
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + resp.json()["access"]

    def test_users_streak(self):
        self._login()
        resp = self.client.get("/users/streak/")
        self.assertEquals(resp.json()["streak_days"], self.streak.days)
        self.assertEquals(200, resp.status_code)
        self.assertEquals(self.user.streak_days, resp.json()["days_complete"])


class UserLevelsTestCase(TestCase):
    email = "testuser@example.com"
    password = "Password1234!"
    data = {}

    def setUp(self):
        self.user = DariasUser.objects.create_user(self.email, password=self.password)
        self.user.first_name = "John"
        self.user.last_name = "Doe"
        self.user.birthday_date = "2021-02-11"
        self.level = Level.objects.create(score=10, points=10)
        self.user.save()

    def _login(self):
        auth_data = {"email": self.email, "password": self.password}
        resp = self.client.post("/auth/jwt/create/", auth_data)
        self.assertEquals(200, resp.status_code)
        self.client.defaults["HTTP_AUTHORIZATION"] = "Bearer " + resp.json()["access"]

    def test_users_levels(self):
        self._login()
        levels = Level.objects.all()
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(len(resp.json()["available_achievements"]), levels.count())
        self.assertEquals(resp.json()["available_achievements"][0]["score"], self.level.score)
        Level.objects.create(score=1, points=1)
        levels = Level.objects.all()
        resp = self.client.get("/users/me/")
        self.assertEquals(200, resp.status_code)
        self.assertEquals(len(resp.json()["available_achievements"]), levels.count())
        self.assertNotEquals(resp.json()["available_achievements"][0]["score"], self.level.score)
