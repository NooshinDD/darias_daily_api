from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import AvatarView, InterestsViewSet, UsersViewSet

router = DefaultRouter()
router.register("", UsersViewSet, InterestsViewSet)
router.register("me/interests", InterestsViewSet)


urlpatterns = [
    path("me/photo/", AvatarView.as_view()),
    path("", include(router.urls)),
]
