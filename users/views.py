from rest_framework import status, views, viewsets
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView

from djoser.compat import get_user_email
from djoser.views import UserViewSet
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from users.models import DariasUser, Interest
from users.serializers import (
    AccountExistenceSerializer,
    AvatarSerializer,
    CustomTokenPairSerializer,
    GoogleSocialAuthSerializer,
    InterestListSerializer,
    InterestSerializer,
    RegistrationSerializer,
    StaticAvatarSerializer,
    StreakSerializer,
)

from core.secret import generate_secret
from core.tasks import send_email_with_password


class UsersViewSet(UserViewSet):
    http_method_names = ["get", "post", "patch", "delete", "head"]

    def create(self, request, **kwargs):
        serializer = RegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)

    def activation(self, request, *args, **kwargs):
        pass

    def resend_activation(self, request, *args, **kwargs):
        pass

    def set_username(self, request, *args, **kwargs):
        pass

    def reset_username_confirm(self, request, *args, **kwargs):
        pass

    def reset_username(self, request, *args, **kwargs):
        pass

    def reset_password_confirm(self, request, *args, **kwargs):
        pass

    @action(["post"], detail=False)
    def reset_password(self, request, *args, **kwargs):
        serializer = AccountExistenceSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data.get("email").lower()
        user = DariasUser.objects.filter(email=email).first()
        if user:
            new_password = generate_secret(10)
            user.set_password(new_password)
            user.save()
            to = [get_user_email(user)]
            send_email_with_password.delay(new_password, to)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "email": openapi.Schema(type=openapi.TYPE_STRING),
            },
        ),
    )
    @action(
        ["post"],
        detail=False,
        permission_classes=[
            AllowAny,
        ],
    )
    def exists(self, request, *args, **kwargs):
        serializer = AccountExistenceSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data.get("email").lower()
        data = {"exists": DariasUser.objects.filter(email=email).exists()}
        return Response(data)

    @action(
        ["get"],
        detail=False,
    )
    def streak(self, request, *args, **kwargs):
        user = self.queryset.filter(pk=request.user.pk).prefetch_related("streaks").first()
        serializer = StreakSerializer(instance=user)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "photo": openapi.Schema(type=openapi.TYPE_STRING),
            },
        ),
    )
    @action(
        ["post"],
        detail=False,
    )
    def static_avatar(self, request, *args, **kwargs):
        """
        Static avatar creation.

        Available Avatar names:


            {
                'bear',
                'elf',
                'whale',
                'bird'
            }
        """
        serializer = StaticAvatarSerializer(data=request.data, instance=request.user)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(status=status.HTTP_200_OK)


class AvatarView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (
        MultiPartParser,
        FormParser,
    )
    serializer_class = AvatarSerializer

    def post(self, request, *args, **kwargs):
        serializer = AvatarSerializer(data=request.FILES, instance=request.user)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(status=status.HTTP_200_OK)


class InterestsViewSet(viewsets.GenericViewSet):
    http_method_names = ("post", "get")
    queryset = Interest.objects.filter(is_active=True)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = InterestSerializer

    def list(self, request, *args, **kwargs):
        """gives list of active interests"""
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "interests": openapi.Schema(
                    type=openapi.TYPE_ARRAY,
                    items=openapi.Schema(
                        type=openapi.TYPE_OBJECT,
                        properties={"id": openapi.Schema(type=openapi.TYPE_INTEGER)},
                    ),
                    description="List of interests id, Empty list will erase all interests",
                ),
            },
        ),
    )
    def create(self, request, *args, **kwargs):
        serializer = InterestListSerializer(
            data=request.data,
            instance=request.user,
        )
        serializer.is_valid(raise_exception=True)
        try:
            serializer.save()
        except Interest.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class TokenPairView(TokenObtainPairView):
    serializer_class = CustomTokenPairSerializer


class GoogleOAuth2View(views.APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "auth_token": openapi.Schema(type=openapi.TYPE_STRING),
            },
        ),
    )
    def post(self, request, *args, **kwargs):
        serializer_class = GoogleSocialAuthSerializer
        serializer = serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data["auth_token"]
        return Response(data, status=status.HTTP_200_OK)
